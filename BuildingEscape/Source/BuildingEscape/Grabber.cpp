// (C) 2019 Stormtek Games

#include "Grabber.h"
#include "Engine/World.h"
#include "Engine/Classes/Components/PrimitiveComponent.h"
#include "GameFramework/PlayerController.h"
#include "DrawDebugHelpers.h"

#define OUT

// Sets default values for this component's properties
UGrabber::UGrabber() {

	PrimaryComponentTick.bCanEverTick = true;
}

// Called when the game starts
void UGrabber::BeginPlay() {

	Super::BeginPlay();
	FindPhysicsHandleComponent();
	SetupInputComponent();	
}

// Look for attached PhysicsHandle
void UGrabber::FindPhysicsHandleComponent() {

	PhysicsHandle = GetOwner()->FindComponentByClass<UPhysicsHandleComponent>();

	if (PhysicsHandle) {

	}
	else {
		UE_LOG(LogTemp, Error, TEXT("%s has no PyhsicsHandle component attached!"), *GetOwner()->GetName());
	}
}

// Look for attached InputComponent
void UGrabber::SetupInputComponent()
{	
	InputComponent = GetOwner()->FindComponentByClass<UInputComponent>();

	if (InputComponent) {
		/// Bind the input actions
		InputComponent->BindAction("Grab", IE_Pressed, this, &UGrabber::Grab);
		InputComponent->BindAction("Grab", IE_Released, this, &UGrabber::Release);
	}
	else {
		UE_LOG(LogTemp, Error, TEXT("%s has no InputComponent attached!"), *GetOwner()->GetName());
	}
}

// Returns the end of the reach line (and the start location as an out parameter)
FVector UGrabber::GetReachLineEnd(FVector &OutLocation) {

	FRotator PlayerViewPointRotation;
	GetWorld()->GetFirstPlayerController()->GetPlayerViewPoint(
		OUT OutLocation,
		OUT PlayerViewPointRotation
	);
	return OutLocation + (PlayerViewPointRotation.Vector() * Reach);
}

void UGrabber::Grab() {

	/// Line trace and see if we reach any actors with physics body collision channel set
	auto HitResult = GetFirstPhysicsBodyInReach();
	auto ComponentToGrab = HitResult.GetComponent();
    auto ActorHit = HitResult.GetActor();
	/// If we hit something then attach a physics handle	
	if (ActorHit) {
		/// Attach the physics handle
		PhysicsHandle->GrabComponent(
			ComponentToGrab,
			NAME_None,
			ComponentToGrab->GetOwner()->GetActorLocation(),
			true
		);
	}
}

void UGrabber::Release() {

	/// Release the physics handle
	PhysicsHandle->ReleaseComponent();
}

// Called every frame
void UGrabber::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) {

	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	/// Set target location of the grabbed component
	if (PhysicsHandle->GrabbedComponent) {		
		FVector PlayerViewPointLocation;
		FVector LineTraceEnd = GetReachLineEnd(OUT PlayerViewPointLocation);
		PhysicsHandle->SetTargetLocation(LineTraceEnd);
	}
}

FHitResult UGrabber::GetFirstPhysicsBodyInReach() {

	/// Setup collision query params
	FCollisionQueryParams CollisionQueryParams = { FName(TEXT("")), false, GetOwner() };
	/// Line-trace (Ray-cast) out to reach distance
	FVector PlayerViewPointLocation;
	FVector LineTraceEnd = GetReachLineEnd(OUT PlayerViewPointLocation);
	FHitResult HitResult;
	GetWorld()->LineTraceSingleByObjectType(
		OUT HitResult,
		PlayerViewPointLocation,
		LineTraceEnd,
		FCollisionObjectQueryParams(ECollisionChannel::ECC_PhysicsBody),
		CollisionQueryParams);
	/// See what we hit
	AActor* ActorHit = HitResult.GetActor();
	if (ActorHit) {
		UE_LOG(LogTemp, Warning, TEXT("Collision Actor: %s"), *(ActorHit->GetName()));
	}
	return HitResult;
}
